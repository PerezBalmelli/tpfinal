    Nodo* lista = NULL;
    Alumno alumno1,alumno2,alumno3;
    strcpy(alumno1.nombre,"Martinez Rosa");
    strcpy(alumno2.nombre,"Rodriguez Ricardo");
    strcpy(alumno3.nombre,"Rodriguez Ana");

    //Asignacion de edad
    alumno1.edad = 20;
    alumno2.edad = 21;
    alumno3.edad = 32;

    agregarElemento(&lista,alumno1);
    agregarElemento(&lista,alumno2);
    agregarElemento(&lista,alumno3);
    imprimir(lista); //SE MUESTRAN LOS ALUMNOS ORDENADOS ALFABETICAMENTE

    //eliminarElementoPorValor(&lista,"Rodriguez Ricardo");
    //eliminarElementoPorPosicion(&lista,1); //falta completar que pasa si no se encuentra posicion
    //printf("%d\n",obtenerLargoDeLaLista(lista));
    //printf("%s\n",obtenerElemento(lista,2));
    //imprimir(lista);