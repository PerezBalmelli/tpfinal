// - - - MATERIAS - - -


typedef struct NodoMateria{

    Materia * materia;
    struct NodoMateria * siguiente;

}NodoMateria;

typedef struct LSEMaterias {

    NodoMateria * cabeza;
    NodoMateria * cola;
    int longitud;

}LSEMaterias;

/*
 * Recorre la lista de materias e imprime por consola una por una.
 * fID:3
 */
void LSEimprimirMaterias(LSEMaterias * lista) {

    //-- LOG --
    clock_t start, end;
    double cpu_time_used;
    start = clock();

    int i;
    NodoMateria * nodo;
    nodo = lista->cabeza;

    for(i = 0; i < lista->longitud; i++){

        printf("ID: %d\nNombre: %s\n", nodo->materia->id,nodo->materia->nombre);
        nodo = nodo->siguiente;

    }

    //-- LOG --
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    ftlog(3,cpu_time_used);

}

/*
 * Crea una lista de materias inicializada y retorna su puntero.
 * fID: 4
 */
LSEMaterias * inicializarLSEMaterias() {
    //-- LOG --
    clock_t start, end;
    double cpu_time_used;
    start = clock();

    LSEMaterias * lista = (LSEMaterias *)malloc(sizeof(LSEMaterias));
    lista->longitud = 0;
    lista->cabeza = NULL;
    lista->cola = NULL;

    //-- LOG --
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    ftlog(4,cpu_time_used);

    return lista;
}

/*
 * Recorre la lista y devuelve el puntero a la materia correspondiente a su posicion en la lista.
 * fID: 2
 */
Materia * LSEobtenerMateriaEnPosicion(LSEMaterias * lista, int posicion) {

    //-- LOG --
    clock_t start, end;
    double cpu_time_used;
    start = clock();

    int i;
    NodoMateria * nodo;
    nodo = lista->cabeza;

    for(i = 0; i < posicion; i++){

        nodo = nodo->siguiente;

    }

    //-- LOG --
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    ftlog(2,cpu_time_used);

    return nodo->materia;
}

/*
 * Crea una materia, la inserta en un nodo y lo posiciona al final de la lista.
 * fID: 1
 */
void LSEagregarMateria(LSEMaterias * lista, int idMateria, char nombreMateria[30], LSEMaterias * correlativas) {

    //-- LOG --
    clock_t start, end;
    double cpu_time_used;
    start = clock();

    //INICIALIZACION (LISTA VACIA)
    if (lista->longitud == 0){

        //CREA LA MATERIA
        Materia * materia = (Materia *)malloc(sizeof(Materia));
        strcpy(materia->nombre,nombreMateria);
        materia->id = idMateria;
        materia->correlativas = correlativas;

        //CREA EL NODO CON LA MATERIA
        NodoMateria * cabeza = (NodoMateria *)malloc(sizeof(NodoMateria));
        cabeza->siguiente = NULL;
        cabeza->materia = materia;

        //ASIGNA EL NODO A LA CABEZA Y COLA DE LA LISTA
        lista->cabeza = cabeza;
        lista->cola = lista->cabeza;
        lista->longitud = 1;

    }
        //AGREGADO A LISTA YA INICIADA
    else {

        //CREA LA MATERIA
        Materia * materia = (Materia *)malloc(sizeof(Materia));
        strcpy(materia->nombre,nombreMateria);
        materia->id = idMateria;
        materia->correlativas = correlativas;

        //CREA EL NODO CON LA MATERIA
        NodoMateria * nuevoNodo = (NodoMateria *)malloc(sizeof(NodoMateria));
        nuevoNodo->siguiente = NULL;
        nuevoNodo->materia = materia;

        //INSERTA EL NODO EN LA COLA DE LA LISTA
        NodoMateria * nodo = lista->cola;
        nodo->siguiente = nuevoNodo;
        lista->cola = nuevoNodo;
        lista->longitud++;

    }

    //-- LOG --
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    ftlog(1,cpu_time_used);
}
