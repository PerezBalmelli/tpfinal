
#ifndef TPLISTA_GESTIONALUMNOS_H
#define TPLISTA_GESTIONALUMNOS_H

typedef struct{
    int id;
    char * nombre;
    struct ADMaterias * correlativas;
}Materia;

typedef struct{
    char nombre[20];
    int edad;
    struct LSEIDs * materiasEnCurso;
    struct LSEIDs * materiasAprobadas;
}Alumno;
#endif //TPLISTA_GESTIONALUMNOS_H
