#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "gestionAlumnos.h"


// - - - FUNCIONES PARA LOG - - -


/*
 * Obtiene la hora actual.
 * fID: 98
 */
char * ahora() {
    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );

    return asctime (timeinfo);
}

/*
 * Loguea en un archivo la ejecucion de las funciones en el programa.
 * fID: 99
 */
void ftlog(int idFuncion, double tiempoTranscurrido) {
    FILE * log;
    log = fopen("../flog/ftlog.log", "a");
    if(log)
    {
        fprintf(log,"--fID %d in %f on -%s", idFuncion, tiempoTranscurrido, ahora());
        fclose(log);
    }
    else
    {
        printf("\nlog error on %s",ahora());
    }
}

/*
 * --OBLIGATORIO TODO: Lista enlazada de numeros para guardar los IDs de las materias de cada alumno
 * --OBLIGATORIO TODO: Implementar una resolucion de nombre-ID para las materias aprobadas y en curso por cada alumno
 * TODO: Agregar try-catch y logueo de errores
 * TODO: Lectura y guardado de datos (csv)
 * TODO: Backups del log y del archivo principal
 * TODO: Shrink de log
 * TODO: Busqueda optimizada (Indices)
 * TODO: Archivo de configuraciones (Elegir si loguear, auto-shrink, compresion, etc)
 * TODO: Funcion de impresion de arbol de correlatividades
 * --OBLIGATORIO TODO: Interfaz para usuario
 * --OBLIGATORIO TODO: Pruebas y limitaciones temporales y espaciales
 * TODO: Programa en PY para estadisticas (Pandas, Seaborn)
 * --OBLIGATORIO TODO: Implementar arreglo de tamanio variable a materias (analizar alumnos) (No funciona implementacion como se espera)
 * --OBLIGATORIO TODO: Buscar como pasar un arreglo por parametro y asignarlo
 */

// - - - Arreglo dinamico de Materias - - -

typedef struct ADMaterias {
    int cantidadElementos;
    int capacidad;
    Materia * materias;
}ADMaterias;

ADMaterias * inicializarADMaterias() {

    //-- LOG --
    clock_t start, end;
    double cpu_time_used;
    start = clock();

    ADMaterias * arreglo = (ADMaterias *)malloc(sizeof(ADMaterias));
    arreglo->cantidadElementos = 0;
    Materia * materias = (Materia *)malloc(sizeof(Materia *));
    arreglo->materias = materias;
    arreglo->capacidad = 20;

    //-- LOG --
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    ftlog(4,cpu_time_used);

    return arreglo;

}

void imprimirMaterias(ADMaterias * arreglo){

    int i;
    Materia * materia;
    for(i = 0; i<arreglo->cantidadElementos;i++){

        materia = & arreglo->materias[i];
        printf("ID: %d\n", materia->id);
        printf("Nombre: %s\n", materia->nombre);
    }

}

void redimensionarArreglo(ADMaterias * arreglo){

    //-- LOG --
    clock_t start, end;
    double cpu_time_used;
    start = clock();

    Materia * nuevoArreglo = (Materia *)calloc(arreglo->capacidad+20,sizeof(Materia));

    int i;
    for(i = 0; i<arreglo->cantidadElementos;i++){

        nuevoArreglo[i] = arreglo->materias[i];

    }

    free(arreglo->materias);
    arreglo->materias = nuevoArreglo;
    arreglo->capacidad = arreglo->cantidadElementos+20;

    //-- LOG --
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    ftlog(4,cpu_time_used);

}

void agregarMateria(ADMaterias * arreglo, char * nombre, ADMaterias * correlativas){

    //SI LLEGO A SU MAXIMA CAPACIDAD, LO REDIMENSIONA
//    if(arreglo->cantidadElementos == arreglo->capacidad){
//        redimensionarArreglo(arreglo);
//    }

    //CREA LA MATERIA
    Materia * materia = (Materia *)malloc(sizeof(Materia));
    materia->nombre = nombre;
    materia->correlativas = correlativas;
    materia->id = arreglo->cantidadElementos;

    //LA POSICIONA EN EL ARREGLO
    arreglo->materias[arreglo->cantidadElementos] = *materia;
    arreglo->cantidadElementos++;

}

// - - - LSE de IDs - - -

typedef struct NodoID {
    int id;
    struct NodoID * siguiente;
}NodoID;

typedef struct LSEIDs{
    NodoID * cabeza;
    NodoID * cola;
    int longitud;
}LSEIDs;

/*
 * Crea una lista de ids inicializada y retorna su puntero.
 * fID: 6
 */
LSEIDs * inicializarLSEIDs() {
    //-- LOG --
    clock_t start, end;
    double cpu_time_used;
    start = clock();

    LSEIDs * lista = (LSEIDs *)malloc(sizeof(LSEIDs));
    lista->longitud = 0;
    lista->cabeza = NULL;
    lista->cola = NULL;

    //-- LOG --
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    ftlog(6,cpu_time_used);

    return lista;
}

/*
 * Crea un nodo con el id y lo posiciona al final de la lista.
 * fID: 7
 */
void agregarID(LSEIDs * lista, int id) {

    //-- LOG --
    clock_t start, end;
    double cpu_time_used;
    start = clock();

    //INICIALIZACION (LISTA VACIA)
    if (lista->longitud == 0){

        //CREA EL NODO CON EL ID
        NodoID * cabeza = (NodoID *)malloc(sizeof(NodoID));
        cabeza->siguiente = NULL;
        cabeza->id = id;

        //ASIGNA EL NODO A LA CABEZA Y COLA DE LA LISTA
        lista->cabeza = cabeza;
        lista->cola = lista->cabeza;
        lista->longitud = 1;

    }
        //AGREGADO A LISTA YA INICIADA
    else {

        //CREA EL NODO CON EL ID
        NodoID * nuevoNodo = (NodoID *)malloc(sizeof(NodoID));
        nuevoNodo->siguiente = NULL;
        nuevoNodo->id = id;

        //INSERTA EL NODO EN LA COLA DE LA LISTA
        NodoID * nodo = lista->cola;
        nodo->siguiente = nuevoNodo;
        lista->cola = nuevoNodo;
        lista->longitud++;

    }

    //-- LOG --
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    ftlog(7,cpu_time_used);
}

/*
 * Remueve un nodo de la lista y libera el espacio.
 * fID: 8
 */
void removerID(LSEIDs * lista, int id){
    //-- LOG --
    clock_t start, end;
    double cpu_time_used;
    start = clock();

    NodoID * nodo;
    nodo = lista->cabeza;

    while(nodo->siguiente->id =! id){
        nodo = nodo->siguiente;
    }

    // NODO ANTERIOR (nodo) -> NODO A ELIMINAR (nodo->siguiente) -> NODO SIGUIENTE (nodoSiguiente)
    NodoID * nodoSiguiente = nodo->siguiente->siguiente;
    free(nodo->siguiente); //LIBERA ESPACIO EN MEMORIA
    nodo->siguiente = nodoSiguiente;

    //-- LOG --
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    ftlog(8,cpu_time_used);
}


// - - - MAIN - - -


int main() {

    ADMaterias * listadoMaterias = inicializarADMaterias();

    agregarMateria(listadoMaterias,"Analisis Matematico I", NULL);
    agregarMateria(listadoMaterias,"Algebra I",NULL);
    agregarMateria(listadoMaterias,"Algoritmos y Programacion I",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion II",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion III",NULL);
    agregarMateria(listadoMaterias,"Sistemas de Representacion IV",NULL);

    imprimirMaterias(listadoMaterias);

    return 0;
}